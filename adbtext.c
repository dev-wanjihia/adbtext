#include<stdlib.h>
#include<stdio.h>
#include<string.h>

// To compile run this command
/**
*       gcc -o /usr/local/bin/adbtext adbtext.c
*/

int main(int argc, char **argv){
    char *buffer;

    // The number of characters
    int charCount = 21, i = 1;

    // Add the length of each argument and the %s characters
    for(int j = 0; j < argc; j++){
        charCount += strlen(argv[j]) + 2;
    }

    // Allocate memory for the characters 
    buffer = calloc(charCount, sizeof(char));

    strcpy(buffer, "adb shell input text ");

    while(i < argc){
        strcat(buffer, argv[i]);

        if(i != argc - 1)
            strcat(buffer, "%s");

        i++;
    }

   system(buffer);
}