# adbtext

## Introduction

A program to simplify the input of text from the adb command.

The problem I am trying to solve is writing the following

```shell
adb shell input text my%sname%sis%ssamson
```

by writing this instead

```shell
adbtext my name is samson
```

## Prerequisites

Ensure that you have adb added to your path. To add it to your path
just follow these simple instructions.

### For windows 
1. Open your environment variable editor
2. Add a semicolon (;) then the path of adb to your path
3. Save the changes

### For Mac OS
1. Open your terminal
2. Type the following command to create a symbolic link to your adb file to the users bin folder
```shell
ln -s [Path to your adb file] /usr/local/bin/adb
```
3. Open a new terminal and execute adb if it does not show command not found then you are good.

### For linux
1. Open terminal 
2. Type the following command to create a symbolic link to your adb file to the users bin folder
```shell
ln -s [Path to adb file] /usr/local/bin/adb
```
3. Open a new terminal and execute adb if it does not show command not found then you are good.

You can find the binaries for adb on this link

You can find the instructions and the details on how to install adb   [here]: https://www.xda-developers.com/install-adb-windows-macos-linux/ .

## Compiling adbtext
Just download the adbtext.c file save it in your folder and then execute the following command

### For Linux or Mac OS
```shell
gcc -o /usr/local/bin/adbtext adbtext.c
```

###For windows 

Use your favourite compiler 
Move the executable file to C:/Program Files/adbtext/adbtext.exe
Add this path to your environment variable to be able to access this program from anywhere within
your command prompt.


## Usage

1. Enable debug mode on your Android phone
2. Connect your phone to your computer
3. Whenever you need to type something just execute the following command on your computer
```shell
adbtext this is what I want to write
```
4. If all goes well you should see the text being written on your phone
